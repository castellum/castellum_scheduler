Castellum Scheduler -- an open source tool for scheduling appointments

Scheduler allows users to set up a set of timeslots and generate unique
invitation links. Each invited person can then pick a free slot
without needing to log in.

In a way, scheduler is the opposite of doodle: Everyone picks a
different time slot instead of agreeing on a single one.

Invited people can only pick time slots in the future. After that, they
can still change their choice as long as it is in the future. If
necessary, logged in users can pick time slots for them without those
restrictions.

Scheduler is part of the [Castellum][1] project, but can be used with
other tools just as well.

# Installation

## Development

For development, a single `make` will install all dependencies and start
the server. You can log in as "admin" with password "password".

If you want to run the scheduler alongside castellum for development you
need to set the following settings in castellum:

```python
SCHEDULER_URL = 'http://localhost:8001'
SCHEDULER_TOKEN = 'CHANGEME'  # same as API_TOKEN in the scheduler settings
```

## Production

There are many different ways to deploy a django application. Please see
the [django documentation][2] for details. A complete example is also
availabe in the [castellum documentation][3].

Most important steps:

-   Create a settings file and set the `DJANGO_SETTINGS_MODULE`
    environment variable accordingly
-   Provide values `SECRET_KEY` and `API_TOKEN` in the settings file you
    just created
-   Setup a database and add the relevant information to the settings
    file
-   Set the `TITLE` setting to a name that should be displayed on the
    top of the page.
-   Add links to the imprint and data protection information to `NAV`.
    You can either create them with an external CMS or by using [django
    flatpages][5].
-   Configure authentication, e.g. [LDAP][4]

# API

This system is designed so that castellum can remain in a secure
internal network and only the scheduler is accessible from the internet.

All API requests must send an `Authorization` header with the secret
token defined in `settings.API_TOKEN`.

You can use PUT/DELETE requests to create/delete invitations for a
schedule. PUT will always respond with 204. DELETE will respond with 404
if no matching invitation existed.

You can use a GET request to get the currently selected timeslot for an
invitation.

Example:

```sh
$ curl -X PUT -H 'Authorization: token CHANGEME' http://localhost:8001/api/26f4c524-4c74-4788-90c6-889dff86001d/foo/
$ curl -X GET -H 'Authorization: token CHANGEME' http://localhost:8001/api/26f4c524-4c74-4788-90c6-889dff86001d/foo/
{"datetime": "2020-11-03T07:00:00"}
$ curl -X GET -H 'Authorization: token CHANGEME' http://localhost:8001/api/26f4c524-4c74-4788-90c6-889dff86001d/
{"foo": "2020-11-03T07:00:00"}
$ curl -X DELETE -H 'Authorization: token CHANGEME' http://localhost:8001/api/26f4c524-4c74-4788-90c6-889dff86001d/foo/
```

[1]: https://castellum.mpib.berlin
[2]: https://docs.djangoproject.com/en/stable/howto/deployment/
[3]: https://git.mpib-berlin.mpg.de/castellum/castellum/-/tree/main/docs/deployment/example_1
[4]: https://django-auth-ldap.readthedocs.io/en/latest/
[5]: https://docs.djangoproject.com/en/stable/ref/contrib/flatpages/
