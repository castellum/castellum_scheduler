from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from model_bakery import baker

from .main.models import Invitation
from .main.models import Schedule
from .main.models import Timeslot


class TestInvitationApiView(TestCase):
    def setUp(self):
        self.client.defaults['HTTP_AUTHORIZATION'] = 'token ' + settings.API_TOKEN

    def test_get(self):
        invitation = baker.make(Invitation)
        url = f'/api/{invitation.schedule.uuid}/{invitation.token}/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        schedule = baker.make(Schedule)
        url = '/api/{}/{}/'.format(schedule.uuid, 'sometoken')
        response = self.client.put(url)
        self.assertEqual(response.status_code, 204)
        self.assertTrue(Invitation.objects.filter(token='sometoken').exists())

    def test_delete(self):
        invitation = baker.make(Invitation)
        url = f'/api/{invitation.schedule.uuid}/{invitation.token}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertFalse(Invitation.objects.filter(token=invitation.token).exists())


class TestScheduleForm(TestCase):
    def test_create(self):
        user = baker.make(User)
        self.client.force_login(user)
        response = self.client.post('/new/', {
            'title': 'test',
            'advance': 0,
            'timeslots': [
                '1970-01-01 12:00',
                '1970-01-01 13:00 ',
                '1970-01-01 14:00 comment',
                '1970-01-01 15:00 multiple words',
            ],
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(user.schedule_set.count(), 1)
        self.assertEqual(Timeslot.objects.count(), 4)
        self.assertTrue(Timeslot.objects.filter(
            date='1970-01-01', time='12:00', comment=''
        ).exists())
        self.assertTrue(Timeslot.objects.filter(
            date='1970-01-01', time='13:00', comment=''
        ).exists())
        self.assertTrue(Timeslot.objects.filter(
            date='1970-01-01', time='14:00', comment='comment'
        ).exists())
        self.assertTrue(Timeslot.objects.filter(
            date='1970-01-01', time='15:00', comment='multiple words'
        ).exists())
