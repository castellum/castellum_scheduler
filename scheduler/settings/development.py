from .default import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'CHANGEME'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# disable cache busting (necessary for testing)
STORAGES = {
    'default': {
        'BACKEND': 'django.core.files.storage.FileSystemStorage',
    },
    'staticfiles': {
        'BACKEND': 'django.contrib.staticfiles.storage.StaticFilesStorage',
    },
}


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR.parent / 'db.sqlite3',
    }
}

# You can overwrite the primary color and its two darker shades.
# Default: ['#0d6efd', '#0b5ed7', '#0a58ca']
BOOTSTRAP_THEME_COLORS = None

# The title will be displayed in the application header.
TITLE = 'Scheduler'

# Hint: You can use django flatpages to create these pages.
# https://docs.djangoproject.com/en/stable/ref/contrib/flatpages/
NAV = [
    ('/imprint/', 'Imprint'),
    ('/data-protection/', 'Data protection'),
]

# The token that clients will have to send in the Authorization header
# SECURITY WARNING: keep the API token used in production secret!
API_TOKEN = 'CHANGEME'
