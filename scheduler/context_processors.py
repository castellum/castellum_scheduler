from django.conf import settings


def get_settings(request):
    return {
        'TITLE': settings.TITLE,
        'NAV': settings.NAV,
        'BOOTSTRAP_THEME_COLORS': settings.BOOTSTRAP_THEME_COLORS,
    }
