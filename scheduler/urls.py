from bootstrap_colors.views import BootstrapColorsView
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView
from django.contrib.flatpages.views import flatpage
from django.urls import path
from django.urls import re_path
from django.views.decorators.cache import cache_control

from .main.views import InvitationApiView
from .main.views import InvitationUpdateView
from .main.views import ScheduleApiView
from .main.views import ScheduleCreateView
from .main.views import ScheduleDeleteView
from .main.views import ScheduleListView
from .main.views import ScheduleUpdateView

urlpatterns = [
    path('', ScheduleListView.as_view(), name='index'),
    path('new/', ScheduleCreateView.as_view(), name='schedule-create'),
    path('<uuid:uuid>/', ScheduleUpdateView.as_view(), name='schedule-update'),
    path('<uuid:uuid>/delete/', ScheduleDeleteView.as_view(), name='schedule-delete'),
    path(
        'invitations/<uuid:schedule__uuid>/<token>/',
        InvitationUpdateView.as_view(),
        name='invitation',
    ),
    path('api/<uuid:uuid>/', ScheduleApiView.as_view(), name='api-schedule'),
    path(
        'api/<uuid:schedule__uuid>/<token>/',
        InvitationApiView.as_view(),
        name='api-invitation',
    ),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('admin/', admin.site.urls),
    path('colors.css', cache_control(max_age=86400)(
        BootstrapColorsView.as_view()
    ), name='colors'),
    re_path(r'^(?P<url>.*/)$', flatpage),
]
