export var $$ = {
    on: function(el, eventName, selector, fn) {
        el.addEventListener(eventName, function(event) {
            var target = event.target.closest(selector);
            if (target) {
                fn.call(target, event);
            }
        });
    },
    h: function(tag, attrs, children) {
        var el = document.createElement(tag);
        for (const attr in attrs) {
            el.setAttribute(attr, attrs[attr]);
        }
        for (const child of children) {
            el.append(child);
        }
        return el;
    },
};

$$.on(document, 'click', '[data-clipboard]', function() {
    var targetSelector = this.dataset.clipboard;
    var target = document.querySelector(targetSelector);
    if (navigator.clipboard) {
        navigator.clipboard.writeText(target.value || target.href);
    } else {
        target.focus();
        target.setSelectionRange(0, 1000);
        document.execCommand('copy');
    }
});
$$.on(document, 'click', '[data-animation="success"]', function() {
    this.classList.add('animation-active');
    setTimeout(() => {
        this.classList.remove('animation-active');
    }, 800);
});
