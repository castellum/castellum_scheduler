import { $$ } from './utils.js'
import { Select } from './select/select.js'

var lang = document.documentElement.lang;
var dateFormatter = new Intl.DateTimeFormat(lang, {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    weekday: 'short',
});
var timeFormatter = new Intl.DateTimeFormat(lang, {
    hour: '2-digit',
    minute: '2-digit',
});

var formatDate = function(s) {
    return dateFormatter.format(new Date(s));
};

var formatTime = function(s) {
    return timeFormatter.format(new Date('1970-01-01T' + s));
};

var getElementIndex = function(el) {
    var i = 0;
    while (el.previousElementSibling) {
        i += 1;
        el = el.previousElementSibling;
    }
    return i;
};

var createCell = function(date, time, checked) {
    var td = document.createElement('td');
    var label = document.createElement('label');
    var checkbox = document.createElement('input');
    td.className = 'p-0';
    label.className = 'd-block w-100 p-2 text-center';
    label.title = `${formatDate(date)} ${formatTime(time)}`;
    checkbox.type = 'checkbox';
    checkbox.name = 'timeslots';
    checkbox.value = date + ' ' + time;
    checkbox.checked = checked;
    td.append(label);
    label.append(checkbox);
    return td;
};

var createDeleteButton = function(onclick) {
    var button = document.createElement('button');
    button.type = 'button';
    button.className = 'btn btn-sm btn-light';
    button.textContent = '🗑';
    button.title = document.querySelector('#i18n-delete').content.textContent;
    button.setAttribute('aria-label', button.title);
    if (onclick) {
        button.addEventListener('click', onclick);
    }
    return button;
};

var getInsertIndex = function(elements, value, start) {
    var i = start;
    while (i < elements.length && elements[i].dataset.value < value) {
        i += 1;
    }
    if (i < elements.length && elements[i].dataset.value === value) {
        return -1;
    }
    return i;
};

var addDate = function(table, date, checked) {
    var tbody = table.querySelector('tbody');
    var index = getInsertIndex(tbody.children, date, 0);
    if (index === -1) {
        return;
    }

    var tr = document.createElement('tr');
    tr.dataset.value = date;

    for (let col of table.querySelectorAll('thead th')) {
        var time = col.dataset.value;
        var td;
        if (!time) {  // first column
            td = document.createElement('th');
            td.textContent = formatDate(date) + ' ';
            td.append(createDeleteButton(rmRow));
        } else {
            td = createCell(date, time, checked);
        }
        tr.append(td);
    }

    if (index < tbody.children.length) {
        tbody.children[index].before(tr);
    } else {
        tbody.append(tr);
    }
};

var addTime = function(table, time, checked) {
    var index = getInsertIndex(table.querySelectorAll('thead th'), time, 1);
    if (index === -1) {
        return;
    }

    for (let tr of table.querySelectorAll('tr')) {
        var date = tr.dataset.value;
        var td;
        if (!date) {  // first row
            td = document.createElement('th');
            td.className = 'text-center';
            td.dataset.value = time;
            td.textContent = formatTime(time) + ' ';
            td.append(createDeleteButton(rmCol));
        } else {
            td = createCell(date, time, checked);
        }

        if (index < tr.children.length) {
            tr.children[index].before(td);
        } else {
            tr.append(td);
        }
    }
};

var rmRow = function(event) {
    event.target.closest('tr').remove();
};

var rmCol = function(event) {
    var table = event.target.closest('table');
    var th = event.target.closest('th');
    var i = getElementIndex(th);
    table.querySelectorAll('tr').forEach(tr => {
        tr.children[i].remove();
    });
};

var table = document.querySelector('[data-js="multi-datetime"]');
if (table) {
    $$.on(document, 'submit', '#add_date', event => {
        event.preventDefault();
        addDate(table, event.target.input.value, event.target.checked.checked);
    });

    $$.on(document, 'submit', '#add_time', event => {
        event.preventDefault();
        addTime(table, event.target.input.value, event.target.checked.checked);
    });

    for (let th of table.querySelectorAll('thead th')) {
        if (th.dataset.value) {
            th.textContent = formatTime(th.dataset.value) + ' ';
            th.append(createDeleteButton(rmCol));
        }
    }
    for (let tr of table.querySelectorAll('tbody tr')) {
        let th = tr.querySelector('th');
        let date = tr.dataset.value;
        th.textContent = formatDate(date) + ' ';
        th.append(createDeleteButton(rmRow));

        let thead = table.querySelector('thead tr').children;
        let i = 1;
        for (let label of tr.querySelectorAll('label')) {
            let time = thead[i].dataset.value;
            label.title = `${formatDate(date)} ${formatTime(time)}`;
            i += 1;
        }
    }
}

$$.on(document, 'change', '[data-js="multi-datetime-list"] *', event => {
    var row = event.target.closest('.row');
    var hidden = row.querySelector('input[type="hidden"]');
    var date = row.querySelector('input[type="date"]');
    var time = row.querySelector('input[type="time"]');
    var comment = row.querySelector('input[type="text"]');
    hidden.value = `${date.value} ${time.value} ${comment.value}`;
});

$$.on(document, 'click',  '[data-js="multi-datetime-list"] .row button', event => {
    event.target.closest('.row').remove();
});

$$.on(document, 'click', '[data-js="add"]', event => {
    var date = '';
    if (event.target.previousElementSibling) {
        date = event.target.previousElementSibling.querySelector('input[type="date"]').value;
    }

    var row = $$.h('div', {'class': 'row mb-3'}, [
        $$.h('input', {'type': 'hidden', 'name': 'timeslots'}, []),
        $$.h('div', {'class': 'col-md'}, [
            $$.h('input', {'type': 'date', 'class': 'form-control', 'value': date, 'required': ''}, []),
        ]),
        $$.h('div', {'class': 'col-md'}, [
            $$.h('input', {'type': 'time', 'class': 'form-control', 'required': ''}, []),
        ]),
        $$.h('div', {'class': 'col-md'}, [
            $$.h('input', {'type': 'text', 'class': 'form-control', 'maxlength': 64}, []),
        ]),
        $$.h('div', {'class': 'col-md flex-grow-0'}, [
            createDeleteButton(),
        ]),
    ]);
    event.target.before(row);
});

var owners = document.getElementById('id_owners');
new Select(owners, {
    id: 'owners-select',
    inputClass: 'form-control',
    valueClass: 'badge text-bg-secondary',
});
