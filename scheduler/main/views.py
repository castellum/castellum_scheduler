import datetime
from collections import OrderedDict

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db import models
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import View

from .forms import ScheduleForm
from .models import Invitation
from .models import Schedule


class OwnerRequiredMixin(LoginRequiredMixin):
    def get_queryset(self):
        return self.request.user.schedule_set.order_by('pk')


class ScheduleListView(OwnerRequiredMixin, ListView):
    model = Schedule
    paginate_by = 20

    def get_queryset(self):
        return super().get_queryset().order_by('-pk')


class ScheduleFormMixin:
    model = Schedule
    form_class = ScheduleForm

    def get_success_url(self):
        return reverse('schedule-update', args=[self.object.uuid])

    def form_valid(self, form, *args):
        messages.success(self.request, _('Data has been saved.'))
        return super().form_valid(form, *args)


class ScheduleCreateView(LoginRequiredMixin, ScheduleFormMixin, CreateView):
    def form_valid(self, form, *args):
        response = super().form_valid(form, *args)
        form.instance.owners.add(self.request.user)
        return response


class ScheduleUpdateView(OwnerRequiredMixin, ScheduleFormMixin, UpdateView):
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'


class ScheduleDeleteView(OwnerRequiredMixin, DeleteView):
    model = Schedule
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'

    def get_success_url(self):
        return reverse('index')


class InvitationUpdateView(UpdateView):
    model = Invitation
    slug_field = 'token'
    slug_url_kwarg = 'token'
    fields = ['timeslot']

    def get_object(self):
        obj = get_object_or_404(Invitation, **self.kwargs)
        if (
            not self.request.user.is_authenticated
            and obj.timeslot
            and obj.timeslot.date < datetime.date.today()
        ):
            raise Http404
        return obj

    def get_success_url(self):
        return reverse(
            'invitation', args=[self.object.schedule.uuid, self.object.token]
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        q_available = models.Q(invitation=None)
        if not self.request.user.is_authenticated:
            threshold = (
                timezone.localtime(timezone.now())
                + datetime.timedelta(hours=self.object.schedule.advance)
            )
            q_available &= (
                models.Q(date__gt=threshold.date())
                | models.Q(date=threshold.date(), time__gt=threshold.time())
            )

        timeslots = list(self.object.schedule.timeslot_set.filter(
            q_available | models.Q(invitation=self.object)
        ))

        dates = []
        times = []
        for timeslot in timeslots:
            dates.append(timeslot.date)
            times.append(timeslot.time)

        dates = sorted(set(dates))
        times = sorted(set(times))

        table = OrderedDict()
        for date in dates:
            table[date] = OrderedDict()
            for time in times:
                table[date][time] = 0
        for timeslot in timeslots:
            table[timeslot.date][timeslot.time] = timeslot

        context['table'] = {
            'timeslots': table,
            'times': times,
        }

        return context

    def form_valid(self, form, *args):
        timeslot = form.cleaned_data.get('timeslot')
        if timeslot and timeslot.schedule != self.object.schedule:
            messages.error(self.request, _('Invalid timeslot'))
            return self.form_invalid(form)
        response = super().form_valid(form, *args)
        messages.success(self.request, _('Your booking has been saved.'))
        return response


@method_decorator(csrf_exempt, 'dispatch')
class ScheduleApiView(View):
    def dispatch(self, request, *args, **kwargs):
        if request.headers.get('Authorization') != 'token ' + settings.API_TOKEN:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        schedule = get_object_or_404(Schedule, **kwargs)
        return JsonResponse({
            invitation.token: invitation.datetime
            for invitation in schedule.invitation_set.all()
        })


@method_decorator(csrf_exempt, 'dispatch')
class InvitationApiView(View):
    def dispatch(self, request, *args, **kwargs):
        if request.headers.get('Authorization') != 'token ' + settings.API_TOKEN:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        invitation = get_object_or_404(Invitation, **kwargs)
        return JsonResponse({'datetime': invitation.datetime})

    def put(self, request, *args, **kwargs):
        if 'schedule__uuid' in kwargs:
            schedule = get_object_or_404(Schedule, uuid=kwargs.pop('schedule__uuid'))
            kwargs['schedule_id'] = schedule.id
        invitation, _ = Invitation.objects.get_or_create(**kwargs)
        return HttpResponse(status=204)

    def delete(self, request, *args, **kwargs):
        invitation = get_object_or_404(Invitation, **kwargs)
        invitation.delete()
        return HttpResponse(status=204)
