import datetime
from collections import OrderedDict

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Schedule


class MultiDatetimeField(forms.Field):
    widget = forms.MultipleHiddenInput

    def _clean_item(self, item):
        if isinstance(item, tuple):
            return item

        try:
            date, time, *comment = item.split(' ', 2)
            dt = datetime.datetime.strptime(f'{date} {time}', '%Y-%m-%d %H:%M')
        except (ValueError, TypeError):
            raise ValidationError(_('Invalid datetime format'), code='invalid_datetime')
        return dt.date(), dt.time(), ' '.join(comment)

    def clean(self, value):
        if not value:
            return []
        elif not isinstance(value, (list, tuple)):
            raise ValidationError(_('Enter a list of values'), code='invalid_list')

        values = [self._clean_item(item) for item in value]
        dts = [(date, time) for (date, time, _comment) in values]
        if len(dts) != len(set(dts)):
            raise ValidationError(
                _('Duplicate date/time combinations'), code='invalid_unique'
            )

        return values

    def prepare_value(self, value):
        data = [self._clean_item(item) for item in super().prepare_value(value)]

        if data:
            dates, times, _comments = zip(*data)
            dates = sorted(set(dates))
            times = sorted(set(times))
        else:
            dates = []
            times = []

        table = OrderedDict()
        for date in dates:
            table[date] = OrderedDict()
            for time in times:
                table[date][time] = {'checked': False, 'comment': ''}
        for date, time, comment in data:
            table[date][time] = {'checked': True, 'comment': comment}

        return {
            'timeslots': table,
            'times': times,
        }


class ScheduleForm(forms.ModelForm):
    timeslots = MultiDatetimeField()

    class Meta:
        model = Schedule
        fields = ['title', 'advance', 'owners']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            self.fields['timeslots'].initial = list(
                self.instance.timeslot_set.values_list('date', 'time', 'comment')
            )
        else:
            self.fields['owners'].required = False

    def clean(self):
        cleaned_data = super().clean()

        if 'timeslots' in self.cleaned_data and self.instance.pk:
            timeslots = self.cleaned_data['timeslots']
            dts = [(date, time) for (date, time, _comment) in timeslots]
            booked = self.instance.invitation_set.exclude(timeslot=None).values_list(
                'timeslot__date', 'timeslot__time'
            )
            if any(dt not in dts for dt in booked):
                raise ValidationError(_('Booked time slots cannot be removed'))

        return cleaned_data

    def save_timeslots(self, schedule):
        tuples = self.cleaned_data['timeslots']
        new_dict = {(date, time): comment for date, time, comment in tuples}
        old = list(schedule.timeslot_set.all())
        old_dict = {(t.date, t.time): t for t in old}

        for dt, timeslot in old_dict.items():
            if dt not in new_dict:
                timeslot.delete()
            elif timeslot.comment != new_dict[dt]:
                timeslot.comment = new_dict[dt]
                timeslot.save()

        for date, time, comment in tuples:
            if (date, time) not in old_dict:
                schedule.timeslot_set.create(date=date, time=time, comment=comment)

    def save(self):
        schedule = super().save()
        self.save_timeslots(schedule)
        return schedule
