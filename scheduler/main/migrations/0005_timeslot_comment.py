from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0004_schedule_advance'),
    ]

    operations = [
        migrations.AddField(
            model_name='timeslot',
            name='comment',
            field=models.CharField(blank=True, max_length=64, verbose_name='Comment'),
        ),
    ]
