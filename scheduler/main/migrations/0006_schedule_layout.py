from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0005_timeslot_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='schedule',
            name='layout',
            field=models.SmallIntegerField(choices=[(1, 'table'), (2, 'list')], default=1, verbose_name='Layout'),
        ),
    ]
