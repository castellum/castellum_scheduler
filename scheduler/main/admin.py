from django.contrib import admin

from .models import Invitation
from .models import Schedule
from .models import Timeslot


class ScheduleAdmin(admin.ModelAdmin):
    filter_horizontal = ['owners']


admin.site.register(Invitation)
admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(Timeslot)
