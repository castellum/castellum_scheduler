import datetime
import uuid

from django.conf import settings
from django.db import models
from django.utils import formats
from django.utils.translation import gettext_lazy as _


class Schedule(models.Model):
    TABLE = 1
    LIST = 2

    uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)
    title = models.CharField(_('Title'), max_length=254)
    owners = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Owners'),
        help_text=_('Users can only be added once they have logged in at least once.'),
    )
    advance = models.IntegerField(
        _('Minimum required hours between booking and appointment'), default=0
    )
    layout = models.SmallIntegerField(_('Layout'), choices=[
        (TABLE, _('table')),
        (LIST, _('list')),
    ], default=TABLE)

    def __str__(self):
        return self.title


class Timeslot(models.Model):
    schedule = models.ForeignKey(
        Schedule, verbose_name=_('Schedule'), on_delete=models.CASCADE
    )
    # date and time fields are always naive -- which is what we want
    # https://docs.djangoproject.com/en/3.0/topics/i18n/timezones/#naive-and-aware-datetime-objects
    date = models.DateField(_('Date'))
    time = models.TimeField(_('Time'))
    comment = models.CharField(_('Comment'), max_length=64, blank=True)

    class Meta:
        unique_together = ['schedule', 'date', 'time']
        ordering = ['date', 'time']

    def __str__(self):
        return f'{formats.date_format(self.date)} {formats.time_format(self.time)}'

    @property
    def datetime(self):
        return datetime.datetime.combine(self.date, self.time)


class Invitation(models.Model):
    token = models.CharField(_('Token'), max_length=254)
    schedule = models.ForeignKey(
        Schedule, verbose_name=_('Schedule'), on_delete=models.CASCADE
    )
    timeslot = models.OneToOneField(
        Timeslot,
        verbose_name=_('Timeslot'),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        unique_together = ['schedule', 'token']

    def __str__(self):
        return f'{self.schedule}:{self.token}'

    @property
    def datetime(self):
        return self.timeslot.datetime if self.timeslot else None
